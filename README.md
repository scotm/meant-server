# meant-server
Server base for MEANT stack.

### MEANT:

- Mongo DB (no sql)
- Express (node js)
- Angular 2 (js)
- Node JS
- TypeScript (.d.ts)

### Run

`npm install`